﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Example
{
    public static class Colors
    {
        /// <summary>
        /// Samples the supplied list returning the number of points requested.
        /// </summary>
        /// <param name="colors">List of colours.</param>
        /// <param name="numberOfPoints">Number of colours needed in sample.</param>
        /// <returns>A new projected list of colours.</returns>
        public static IList<Color> Sample(this IList<Color> colors, int numberOfPoints)
        {
            var result = new Color[numberOfPoints];

            for (var i = 0; i < numberOfPoints - 1; i++)
            {
                var index = (int)Math.Floor(((float)i / (numberOfPoints - 1)) * colors.Count);
                result[i] = colors[index];
            }

            result[result.Length - 1] = colors[colors.Count - 1];

            return result;
        }
        
        /// <summary>
        /// Blends the list of colours by averaging adjacent colours until the minimum number of shades have been produced.
        /// </summary>
        /// <param name="colors">Seed colour list.</param>
        /// <param name="minimumNumberOfShades">Minimum number of shades to generate.</param>
        /// <returns>List of blended shades.</returns>
        public static IList<Color> Blend(this IList<Color> colors, int minimumNumberOfShades)
        {
            while (colors.Count < minimumNumberOfShades)
            {
                colors = Inflate(colors);
            }

            return colors;
        }

        /// <summary>
        /// Mixes two colours in equal proportions.
        /// </summary>
        /// <param name="color1">First colour.</param>
        /// <param name="color2">Second colour.</param>
        /// <returns>Mixture of supplied colours.</returns>
        public static Color MixColors(Color color1, Color color2)
        {
            return Color.FromArgb(
                Average(color1.R, color2.R),
                Average(color1.G, color2.G),
                Average(color1.B, color2.B));
        }

        private static int Average(byte byte1, byte byte2)
        {
            var ordered = OrderBytes(byte1, byte2);
            return ordered[0] + ((ordered[1] - ordered[0]) / 2);
        }

        private static byte[] OrderBytes(byte byte1, byte byte2)
        {
            return (byte1 < byte2) ? new[] { byte1, byte2 } : new[] { byte2, byte1 };
        }

        private static IList<Color> Inflate(IList<Color> colors)
        {
            var result = new List<Color>();

            for (var i = 0; i < colors.Count - 1; i++)
            {
                var color1 = colors[i];
                var color2 = colors[i + 1];
                var averageColor = MixColors(color1, color2);

                result.AddRange(new[] { color1, averageColor });
            }

            result.Add(colors[colors.Count - 1]);
            return result;
        }
    }
}
