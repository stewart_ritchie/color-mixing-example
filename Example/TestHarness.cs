﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

using Xunit;

namespace Example
{
    public class TestHarness
    {
        [Fact]
        public void MixGreenGoldAndYellow()
        {
            var colors = (new[] { Color.Red, Color.Gold, Color.Green }).Blend(200).Sample(7);

            SaveTestImage(colors, @"c:\temp\test.png");
        }

        private static IList<Color> Inflate(IList<Color> colors)
        {
            var result = new List<Color>();

            for (var i = 0; i < colors.Count - 1; i++)
            {
                var color1 = colors[i];
                var color2 = colors[i + 1];
                var averageColor = Colors.MixColors(color1, color2);

                result.AddRange(new[] { color1, averageColor });
            }

            result.Add(colors[colors.Count - 1]);
            return result;
        }

        private static void ApplyColors(Graphics graphics, IList<Color> colors, int width, int height)
        {
            var blockwidth = ((float)width) / colors.Count;

            for (var i = 0; i < colors.Count; i++)
            {
                var posX = (((float)i) / colors.Count) * width;

                var rect = new RectangleF
                {
                    Size = new SizeF { Width = blockwidth + 1, Height = height },
                    Location = new PointF { X = posX, Y = 0 }
                };

                using (var brush = new SolidBrush(colors[i]))
                {
                    graphics.FillRectangle(brush, rect);
                }
            }
        }

        private static void SetHighQuality(Graphics graphics)
        {
            graphics.SmoothingMode = SmoothingMode.HighQuality;
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
            graphics.CompositingQuality = CompositingQuality.HighQuality;
        }

        private static void SaveImage(Image image, Stream stream)
        {
            using (var copy = new Bitmap(image))
            {
                copy.Save(stream, ImageFormat.Png);
                stream.Flush();
                stream.Position = 0;
            }
        }

        private static void SaveTestImage(IList<Color> colors, string filename)
        {
            const int Width = 350;
            const int Height = 10;

            using (var stream = new FileStream(filename, FileMode.Create))
            using (var bitmap = new Bitmap(Width, Height, PixelFormat.Format32bppPArgb))
            using (var graphics = Graphics.FromImage(bitmap))
            {
                SetHighQuality(graphics);

                graphics.Clear(Color.Black);

                ApplyColors(graphics, colors, Width, Height);

                SaveImage(bitmap, stream);
            }
        }
    }
}
